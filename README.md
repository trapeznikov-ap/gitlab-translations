# Переводы статей из блога GitLab на русский

- [ ] GitLab flow(#1)
- [ ] ...

## Не забывать:

 - После каждого предложения делать перенос строки - так diff-ы будут понятнее. На форматировние же текста это не повлияет. В markdown только двойной перенос конвертится в новый абзац.